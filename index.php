<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Activity</title>
</head>
<body>
	<?php
		function getFullAddress($country, $city, $province, $specificAddress) {
			echo "$specificAddress, $city, $province, $country";
		};

		$country = "philippines";
		$city = "marikina";
		$province = "ncr";
		$specificAddress = "#103 old jp rizal";

		echo "<h2> Full Address </h2>";
		getFullAddress($country, $city, $province, $specificAddress);
	?>
</body>
</html>